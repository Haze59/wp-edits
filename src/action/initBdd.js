import router from "../renderer/router";
import sqlite3 from 'sqlite3';
import {remote} from "electron";
import path from 'path';

let dbPath = path.join(remote.app.getPath('userData'), '/database.db.sqlite');
let imgPath = path.join(remote.app.getPath('userData'), '/img');

const functions = {
    tryConnectBdd: function () {
        let sqlite = sqlite3.verbose();
        let db = new sqlite.Database(dbPath, (err) => {
            if (err) {
                console.error(err.message);
            } else {
                let sqlUserfind = 'SELECT * FROM User';
                db.get(sqlUserfind, (err, row) => {
                    if (err) {
                        console.log('test')
                        let sqlUser = 'CREATE TABLE if not exists User(' +
                            'id             INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,' +
                            'login          TEXT ,' +
                            'password       TEXT ,' +
                            'url_wordpress  TEXT ,' +
                            'image_profil  NONE ,' +
                            'image_bg  NONE ,' +
                            'tutoriel       INTEGER' +
                            ')';
                        db.run(sqlUser);
                        let sqlPage = 'CREATE TABLE if not exists page(' +
                            'id            INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,' +
                            'contenu       TEXT ,' +
                            'statut       TEXT ,' +
                            'read       TEXT ,' +
                            'image_entete  NONE ,' +
                            'titre         TEXT ' +
                            ');'
                        db.run(sqlPage);
                        let sqlImage = 'CREATE TABLE if not exists image_page(' +
                            '    id       INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL ,' +
                            '    image    NONE ,' +
                            '    id_page  INTEGER,' +
                            '    FOREIGN KEY (id_page) REFERENCES page(id)' +
                            '    );'
                        db.run(sqlImage);

                        router.push('/initUser');
                        return console.error(err.message);
                    }
                    else if (row != null) {
                        if (router.currentRoute.name != 'index') {
                            router.push('/');
                        }

                    }
                });

            }
        });

    },
    CheckImgFolder: function () {
        try {
            electronFs.mkdirSync(imgPath)
        } catch (err) {
            if (err.code !== 'EEXIST') {
                console.log('Le dossier existe déjà :)')
            }
        }
    }
}

export default functions



