const mutations = {
    setUrl(state, url) {
        state.wpUrl = url
    },
    setLogin(state, login) {
        state.login = login
    }
}
const actions = {
    setUrlAction({commit}, obj) {
        commit('setUrl', obj)
    },
    setLoginAction({commit}, obj) {
        commit('setLogin', obj)
    }

}
const state = {
    wpUrl: 0,
    login: 0,
}

export default {
    state,
    mutations,
    actions
}