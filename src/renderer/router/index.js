import Vue from 'vue'
import Router from 'vue-router'
import Blog from '../components/blog';
import EcrirePage from '../components/ecrirepage';
import initUser from '../components/initUser';
import LandingPage from '../components/LandingPage';
import Galerie from '../components/galerie';
import showPage from '../components/showPage';

Vue.use(Router)

export default new Router({
    routes: [
        {
            path: '/',
            name: 'index',
            component: require('@/components/index').default
        },
        {
            path:'/initUser',
            name:'initUser',
            component: initUser
        },
        {
            path: '/accueil',
            name: 'accueil',
            component: LandingPage
        },
        {
            path: '/blog/',
            name: 'blog',
            component: Blog
        },
        {
            path: '/ecrirepage',
            name: 'ecrirePage',
            component: EcrirePage
        },
        {
            path: '/galerie',
            name: 'galerie',
            component: Galerie
        },
        {
            path: '/showPage/:id',
            name: 'showPage',
            component: showPage
        },
        {
            path: '/galerie/:name',
            name: 'galerieparam',
            component: Galerie
        }


    ]
})
