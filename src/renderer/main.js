import Vue from 'vue'
import axios from 'axios'
import Vuex from 'vuex'
import VueLazyLoad from 'vue-lazyload'

import App from './App'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import sqlite3 from "sqlite3";
import path from 'path';
import { remote } from 'electron'
import functions from '../action/initBdd.js'

Vue.use(Vuetify)
Vue.use(Vuex);


functions.CheckImgFolder();
functions.tryConnectBdd();


Vue.prototype.$db = new Vue({
    data: {
        filename: path.join(remote.app.getPath('userData'), '/database.db.sqlite'),
        sqlite: sqlite3.verbose()
    }
})

if (!process.env.IS_WEB) Vue.use(require('vue-electron'))
Vue.http = Vue.prototype.$http = axios
Vue.config.productionTip = false
Vue.use(VueLazyLoad);
/* eslint-disable no-new */
new Vue({
    components: {App},
    router,
    store,
    template: '<App/>'
}).$mount('#app')
